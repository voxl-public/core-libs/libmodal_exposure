/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef EXPOSURE_MSV_H
#define EXPOSURE_MSV_H

#include <stdint.h>
#include <string>

struct modal_exposure_msv_config_t
{
	// Defaults are OV7251, need per sensor configurations through config file
	uint32_t 		gain_min;
	uint32_t 		gain_max;
	uint32_t 		exposure_min_us;
	uint32_t 		exposure_max_us;
	uint32_t    exposure_soft_min_us;
	float       exposure_gain_slope;

	// This is the main setpoint of the algorithm
	float 	 		desired_msv;
	float       msv_filter_alpha;
	float       max_saturated_pix_ignore_fraction;

	uint32_t 	  exposure_update_period;
	uint32_t 	  gain_update_period;
	uint32_t 	  display_debug;
};

static void modal_exposure_msv_print_config(modal_exposure_msv_config_t c)
{
	printf("=================MODALAI Auto Exposure Settings==================\n");
	printf("gain_min:                        %d\n",    c.gain_min);
	printf("gain_max:                        %d\n",    c.gain_max);
	printf("exposure_min_us:                 %d\n",    c.exposure_min_us);
	printf("exposure_max_us:                 %d\n",    c.exposure_max_us);
	printf("exposure_soft_min_us:            %d\n",    c.exposure_soft_min_us);
	printf("exposure_gain_slope:             %f\n",    c.exposure_gain_slope);
	printf("desired_msv:                     %f\n",    c.desired_msv);
	printf("msv_filter_alpha:                %f\n",    c.msv_filter_alpha);
	printf("max_sat_pix_ignore_fraction:     %f\n",    c.max_saturated_pix_ignore_fraction);
	printf("exposure_update_period:          %d\n",    c.exposure_update_period);
	printf("gain_update_period:              %d\n",    c.gain_update_period);
	printf("display_debug:                   %s\n",    (c.display_debug ? "yes" : "no"));
	printf("=================================================================\n");
}

class ModalExposureMSV
{
public:
	ModalExposureMSV(modal_exposure_msv_config_t c)
	{
		ae_config = c;
		stored_gain = 0;
		last_exposure_setting_us = 0;
		last_exposure_setting_filt_us = 0;
		gain_last_setting = 0;
		gain_last_setting_filt = 0;

		exp_gain_prod_des_filt = 0;
		exp_gain_prod_des_last = 0;
	}

	void fill_standard_config(modal_exposure_msv_config_t & c, std::string camera_use_case);

	//compute MSV (mean sample value)
	float compute_msv(	uint8_t* image_data,
						uint32_t width,
						uint32_t stride,
						uint32_t height);

	bool update_exposure(
		uint8_t*	image_data,
		uint32_t	width,
		uint32_t	height,
		uint64_t	cur_exposure_ns,
		uint32_t	cur_gain,
		int64_t*	set_exposure_ns,
		int32_t*	set_gain );

	bool update_exposure(
		uint8_t*	image_data,
		uint32_t	width,
		uint32_t	stride,
		uint32_t	height,
		uint64_t	cur_exposure_ns,
		uint32_t	cur_gain,
		int64_t*	set_exposure_ns,
		int32_t*	set_gain );

	modal_exposure_msv_config_t	ae_config;
	int16_t						stored_gain;

private:
	uint32_t counter = 0;
	bool first = true;

	float exp_gain_prod_des_filt;
	float exp_gain_prod_des_last;

	float last_exposure_setting_us;
	float last_exposure_setting_filt_us;

	float gain_last_setting;
	float gain_last_setting_filt;
};

#endif // end #define CONFIG_FILE_H
