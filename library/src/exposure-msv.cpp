/**
 *  @file exposure-hist.cpp
 *  @brief Main file for running the application
 */
/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


// C/C++ Includes
#include <iostream>
#include <signal.h>
#include <cstring>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <math.h>
#include <float.h>

// Package dependencies
#include "exposure-msv.h"

#ifndef MIN
#define MIN(a,b) (((a)<(b))?(a):(b))
#endif

#ifndef MAX
#define MAX(a,b) (((a)>(b))?(a):(b))
#endif

//hardcoded constants
//#define EXPO_MSV_DESIRED_GAIN 1.0f
#define EXPO_MSV_GAIN_SCALE   0.01f

void ModalExposureMSV::fill_standard_config(modal_exposure_msv_config_t & c, std::string camera_use_case)
{
  //populate default configuration.. for now assume use case ov7251_30fps
  c.gain_min                          = 100; //100=1.0x
  c.gain_max                          = 800;
  c.gain_update_period                = 2;  //ov7251 does not like gain being updated every frame at 30 fps
  c.exposure_min_us                   = 50;
  c.exposure_max_us                   = 15000;
  c.exposure_soft_min_us              = 5000;
  c.exposure_gain_slope               = 0.05;
  c.exposure_update_period            = 1;
  c.desired_msv                       = 60.0; //58.0;
  c.msv_filter_alpha                  = 0.6;
  c.max_saturated_pix_ignore_fraction = 0.2;
  c.display_debug                     = 0;
}

#define DECIMATOR 16

float ModalExposureMSV::compute_msv(uint8_t* image_data, uint32_t width, uint32_t stride, uint32_t height)
{
	// The mean that shall be returned

	// Compute the mean
	uint32_t number_of_pixels    = width * height;
	uint64_t accum_pixel_values  = 0;
	uint32_t num_pixels_in_accum = 0;
	uint32_t max_high_sat_count  = (number_of_pixels * ae_config.max_saturated_pix_ignore_fraction) / (DECIMATOR*DECIMATOR);
	uint32_t high_sat_count      = 0;

/*
	for(uint32_t i = 0; i < (number_of_pixels-3); i+=4)
	{
		accum_pixel_values += image_data[i];
		accum_pixel_values += image_data[i+1];
		accum_pixel_values += image_data[i+2];
		accum_pixel_values += image_data[i+3];
	}
*/

	for(uint32_t i = 0; i < height; i+=DECIMATOR)
	{
		uint32_t start = i*stride;
		for(uint32_t j = 0; j < width; j+=DECIMATOR)
		{
			uint32_t k = start + j;

			//ignore saturated values //FIXME: handle this better
			//the idea is that saturated values are going to bring the mean value higher, forcing other pixels to be darker
			//but there is no guarantee that saturated pixels will become unsaturated if brightness is decreased
			int is_saturated = 0;

			// noise or rounding error may make saturated pixels actually report a
			// little lower than 255
			if(image_data[k] >= 253){
				high_sat_count++;
				is_saturated = 1;
			}


			//count pixel if not saturated or we already exceeded maximum number of saturated pixels to ignore
			if((!is_saturated) || (high_sat_count>max_high_sat_count)){
				accum_pixel_values += image_data[k];
				num_pixels_in_accum++;
			}
		}
	}

	// if somehow the entire image was saturated, avoid a divide by 0
	if(num_pixels_in_accum <= 0) num_pixels_in_accum = 1;

	//mean_sample_value = ((double)accum_pixel_values) / number_of_pixels;
	float mean_sample_value = ((double)accum_pixel_values) / num_pixels_in_accum;

	//printf("saturated pixels: %4d max sat: %4d apv: %9d npa: %5d msv: %0.1f\n", high_sat_count, max_high_sat_count, accum_pixel_values, num_pixels_in_accum, mean_sample_value);

	return mean_sample_value;
}

bool ModalExposureMSV::update_exposure(
	uint8_t*	image_data,
	uint32_t  width,
	uint32_t  height,
	uint64_t	cur_exposure_ns,
	uint32_t	cur_gain,
	int64_t* 	set_exposure_ns,
	int32_t* 	set_gain )
{
	return update_exposure(image_data,width,width,height,cur_exposure_ns,cur_gain,set_exposure_ns,set_gain);
}

bool ModalExposureMSV::update_exposure(
	uint8_t*	image_data,
	uint32_t  width,
	uint32_t	stride,
	uint32_t  height,
	uint64_t	cur_exposure_ns,
	uint32_t	cur_gain,
	int64_t* 	set_exposure_ns,
	int32_t* 	set_gain )
{

	const float GAIN_SCALE  = EXPO_MSV_GAIN_SCALE;  //this is just to normalize the gain to 100ISO = 1x. probably not needed

	bool update 		= first; // set to true if values should be updated
	first 					= false;

  if (last_exposure_setting_us==0)
	{
		last_exposure_setting_us      = cur_exposure_ns/1000.0;
		last_exposure_setting_filt_us = last_exposure_setting_us;
		gain_last_setting             = cur_gain * GAIN_SCALE;
		gain_last_setting_filt        = cur_gain * GAIN_SCALE;
	}

	float mean_sample_value	= 0;

	mean_sample_value = compute_msv(image_data, width, stride, height);

  float exposure_us_now   = cur_exposure_ns/1000.0;
	float gain_now          = cur_gain * GAIN_SCALE;
  float exp_gain_prod_now = exposure_us_now * gain_now;
	float msv_mult          = ae_config.desired_msv / mean_sample_value;
	float exp_gain_prod_des = exp_gain_prod_now * msv_mult;
	float exp_gain_prod_min = ae_config.exposure_min_us * (ae_config.gain_min*GAIN_SCALE);
	float exp_gain_prod_max = ae_config.exposure_max_us * (ae_config.gain_max*GAIN_SCALE);

	exp_gain_prod_des       = MIN(exp_gain_prod_des,exp_gain_prod_max);
	exp_gain_prod_des       = MAX(exp_gain_prod_des,exp_gain_prod_min);

	float filt_alpha        = ae_config.msv_filter_alpha;
	if (exp_gain_prod_des_filt==0)
	{
	  exp_gain_prod_des_filt = exp_gain_prod_des;
		exp_gain_prod_des_last = exp_gain_prod_des;
	}
	exp_gain_prod_des_filt  = exp_gain_prod_des_filt * filt_alpha + exp_gain_prod_des * (1.0-filt_alpha);


/*
  //gain priority
	float gain_des          = EXPO_MSV_DESIRED_GAIN;
	float exposure_us_des   = exp_gain_prod_des_filt / gain_des;
  float exposure_mult     = 1.0;
	float gain_mult         = 1.0;

	if (exposure_us_des > ae_config.exposure_max_us)
	{
		gain_mult       = exposure_us_des / ae_config.exposure_max_us;
	  exposure_us_des = ae_config.exposure_max_us;
	}

	if (exposure_us_des < ae_config.exposure_min_us)
	{
		gain_mult       = exposure_us_des / ae_config.exposure_min_us;
		exposure_us_des = ae_config.exposure_min_us;
	}
*/


  //exposure priority
  float exposure_us_des   = ae_config.exposure_soft_min_us;
  float exp_to_gain_ratio = ae_config.exposure_gain_slope;

  if (exp_gain_prod_des_filt > exposure_us_des)
  {
    exposure_us_des = exposure_us_des + (exp_gain_prod_des_filt - exposure_us_des) * exp_to_gain_ratio;
    exposure_us_des = MIN(exposure_us_des,ae_config.exposure_max_us);
  }

  float gain_des        = exp_gain_prod_des_filt / exposure_us_des;
  float exposure_mult   = 1.0;
  float gain_mult       = 1.0;

  if (gain_des > (ae_config.gain_max*GAIN_SCALE))
	{
    exposure_mult = gain_des / (ae_config.gain_max*GAIN_SCALE);
    gain_des      = ae_config.gain_max*GAIN_SCALE;
	}

  if (gain_des < (ae_config.gain_min*GAIN_SCALE))
	{
    exposure_mult = gain_des / (ae_config.gain_min*GAIN_SCALE);
    gain_des      = ae_config.gain_min*GAIN_SCALE;
	}

	float gain_new        = gain_des * gain_mult;
  float exposure_us_new = exposure_us_des * exposure_mult;

	//gain_new = MIN(gain_new,ae_config.gain_max*GAIN_SCALE);  //should not be needed?
	//gain_new = MAX(gain_new,ae_config.gain_min*GAIN_SCALE);

  *set_exposure_ns = exposure_us_new * 1000;  //convert from us to ns
	*set_gain        = gain_new / GAIN_SCALE;

  //hack for ov7251 which does not like gain being updated every frame at 30fps
	if (counter % ae_config.gain_update_period != 0)
	{
		*set_gain = gain_last_setting / GAIN_SCALE;
	}

	gain_last_setting        = gain_new;
	last_exposure_setting_us = exposure_us_des;

  //expected MSV based on current msv, current gain, current exposure and desired gain, exposure
	float expected_msv = (exposure_us_new*gain_new) / (cur_exposure_ns/1000.0 * cur_gain * GAIN_SCALE) * mean_sample_value;

	//ae_config.display_debug = 1;
	if (ae_config.display_debug &&  (update || counter%1 == 0))
	{
#ifdef __ANDROID__
		LOGI( "Auto Exposure Update [%d]:\n",counter);
		LOGI( "MSV      : curr: %.3f, goal: %.3f, exp: %.3f\n", mean_sample_value, ae_config.desired_msv, expected_msv);
		LOGI( "Exposure : curr: %uus, new: %.3fus\n", (uint32_t)(cur_exposure_ns/1000), exposure_us_new);
		LOGI( "Gain     : curr: %u,   new: %u\n", cur_gain, *set_gain);
		LOGI( "----------");
#else
		printf( "Auto Exposure Update [%d]:\n",counter);
		printf( "MSV      : curr: %.3f, goal: %.3f, exp: %.3f\n", mean_sample_value, ae_config.desired_msv, expected_msv);
		printf( "Exposure : curr: %uus, new: %.3fus\n", (uint32_t)(cur_exposure_ns/1000), exposure_us_new);
		printf( "Gain     : curr: %u,   new: %u\n", cur_gain, *set_gain);
		printf( "----------");

#endif
	}

	counter++;

	update = true;

	return update;
}
